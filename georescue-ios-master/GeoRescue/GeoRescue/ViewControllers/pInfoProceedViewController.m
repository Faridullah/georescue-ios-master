//
//  pInfoProceedViewController.m
//  GeoRescue
//
//  Created by faridullah on 21/05/2015.
//  Copyright (c) 2015 magmasys. All rights reserved.
//

#import "pInfoProceedViewController.h"
#import "offLineMapViewController.h"
@interface pInfoProceedViewController ()

@end

@implementation pInfoProceedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)proceedBtnAction:(id)sender {
    offLineMapViewController *obj1Vc = [[offLineMapViewController alloc]initWithNibName:@"offLineMapViewController" bundle:nil];
    [self.navigationController pushViewController:obj1Vc animated:YES];
}
@end
