//
//  UserDefaultsHelper.m
//  Pet Finder
//
//  Created by Asim Mushtaq on 9/23/13.
//  Copyright (c) 2013 GeoSpat. All rights reserved.
//

#import "UserDefaultsHelper.h"
#import "Constants.h"


@implementation UserDefaultsHelper


+ (void)setFirstProfileAdded:(BOOL)num
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:num] forKey:kSettFirstPetAdded];
}
+ (BOOL)getFirstProfileAdded
{
    NSNumber* num = [[NSUserDefaults standardUserDefaults] objectForKey:kSettFirstPetAdded];
    
    if (!num)
    {
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:NO] forKey:kSettFirstPetAdded];
        return NO;
    }
    
    return num.boolValue;
}

+ (void)setSignUpCompleted:(BOOL)num
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:num] forKey:kSettUserhaveSignedUp];
}
+ (BOOL)getSignUpCompleted
{
    NSNumber* num = [[NSUserDefaults standardUserDefaults] objectForKey:kSettUserhaveSignedUp];
    if (!num)
    {
        num = [NSNumber numberWithBool:NO];
        [[NSUserDefaults standardUserDefaults] setObject:num forKey:kSettUserhaveSignedUp];
    }
    return num.boolValue;
}

+ (void)setUserName:(NSString*)name
{
    [[NSUserDefaults standardUserDefaults] setObject:name forKey:kSettUserName];
}
+ (NSString*)getUserName
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kSettUserName];
}

+ (void)setEmailAddress:(NSString*)email
{
    [[NSUserDefaults standardUserDefaults] setObject:email forKey:kSettEmailAddr];
}
+ (NSString*)getEmailAddress
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kSettEmailAddr];
}

+ (void)setMobileNumber:(NSString*)num
{
    [[NSUserDefaults standardUserDefaults] setObject:num forKey:kSettMobileNum];
}
+ (NSString*)getMobileNumber
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kSettMobileNum];
}

+ (void)setDeviceToken:(NSString*)token
{
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:kSettDeviceToken];
}
+ (NSString*)getDeviceToken
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:kSettDeviceToken];
}

+ (void)setHomeLatitude:(double)lat
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithDouble:lat] forKey:kSettHomeLatitude];
}
+ (double)getHomeLatitude
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:kSettHomeLatitude] doubleValue];
}

+ (void)setHomeLongtude:(double)lon
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithDouble:lon] forKey:kSettHomeLongitude];
}
+ (double)getHomeLongitude
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:kSettHomeLongitude] doubleValue];
}

+ (void)setUserId:(NSString*)strID
{
    [[NSUserDefaults standardUserDefaults] setObject:strID forKey:@"UserID"];
}
+ (NSString*)getUserId
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"];
}

+ (void)setUserImage:(NSString *)strImage
{
    [[NSUserDefaults standardUserDefaults] setObject:strImage forKey:@"UserImage"];
}
+ (NSString*)getUserImage
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"UserImage"];
}

+ (void)setPassword:(NSString *)strPassword
{
    [[NSUserDefaults standardUserDefaults] setObject:strPassword forKey:@"UserPassword"];
}
+ (NSString*)getPassword
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"UserPassword"];
}

+ (void)setRememberMe:(NSString*)ans
{
    [[NSUserDefaults standardUserDefaults] setObject:ans forKey:@"RememberMe"];
}
+ (NSString*)getRememberMe
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"RememberMe"];
}

+ (void)setFirstScreen:(NSString*)ans
{
    [[NSUserDefaults standardUserDefaults] setObject:ans forKey:@"FS"];
}
+ (NSString*)getFirstScreen
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"FS"];
}

@end


